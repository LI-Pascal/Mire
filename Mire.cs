﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Configuration;
using static Alma.NetWrappers.CutPartFactory;
using Agi34;
using System.Threading;

namespace Actcut
{
    class Mire
    {
        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewP01Importer@4")]
        public static extern P01ImporterPtr NewP01Importer(string filename);
        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_IsOnProfile@32")]
        public static extern int IsOnProfile(PartFactoryPtr part_factory_ptr, double x, double y, double distance, bool include_text);
        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewP01PartFactory@4")]
        public static extern PartFactoryPtr NewP01PartFactory(P01ImporterPtr importer);

        static string logFolder = DefineAph.GetKeyPath("log");
        static string pfile = "";
        static LogFile logFile = new LogFile(logFolder + "Log_MireResult.txt");
        static LogFile resultFile = new LogFile(logFolder + "MireResult.txt");

        static double DimMire = 0;
        static double CoefMinPart = 0;
        static double MinDistPart = 0;
        static double StepIteration = 0;
        static double MinDistMire = 0;
        static double lgutil = -1;
        static double sheetHeight = -1;
        static string decimaSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        static void Main(string[] args)
        {

            var executablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            Configuration config = ConfigurationManager.OpenExeConfiguration(executablePath);
            Console.Clear();
            if (args.Length == 1 && args[0].ToLower() == "-redo")
            {
                if (config.AppSettings.Settings["agiFileName"] != null && config.AppSettings.Settings["nestKey"] != null)
                    args = new string[] { config.AppSettings.Settings["agiFileName"].Value, config.AppSettings.Settings["nestKey"].Value };
                if (args.Length == 2)
                {
                    Console.WriteLine("relaunch Mire.exe \"" + args[0] + "\" \"" + args[1] + "\"");
                }
            }

            if (args.Length != 2)
            {
                Console.WriteLine("Mire.exe AgiFullPath KeyNestNumber");
                Console.WriteLine("Mire.exe \"c:\\alma\\data\\Laser\\Rin\\123.agi\" \"KEY_NEST_1\"");
                Console.WriteLine("Mire.exe -redo");
                Console.WriteLine("Appuyez une touche pour continuer . . .");
                Console.ReadKey(true);
                return;
            }

            List<string> errorCfgFile = new List<string>();

            if (!File.Exists(args[0]))
            {
                errorCfgFile.Add("Mire.exe \"" + args[0] + "\" \"" + args[1] + "\"");
                errorCfgFile.Add(args[0] + " fichier introuvable");
                ViewLog(errorCfgFile);
                return;
            }

            string tmpStr = string.Empty;
            string cfgFile = DefineAph.GetRscPath() + "MireParams.cfg";
            string processFile = DefineAph.GetRscPath() + "inprocess.fic";
            File.WriteAllText(processFile, Environment.UserName + " - " + DateTime.Now.ToString());


            #region CfgFile Control
            if (!File.Exists(cfgFile))
            {
                errorCfgFile.Add(cfgFile + " non trouvé dans la ressource");
                ViewLog(errorCfgFile);
                return;
            }

            IniFile iniFile = new IniFile(cfgFile);

            //if (!double.TryParse("10.5", out DimMire))
            //    errorCfgFile.Add("Définir le point comme séparateur décimal");

            if (!iniFile.KeyExists("DimMire", "Params"))
                errorCfgFile.Add("DimMire clé introuvable");
            else
            {
                DimMire = MyCdbl(iniFile.Read("DimMire", "Params"));
                if (DimMire == 0)
                    errorCfgFile.Add("DimMire valeur erronée");
            }

            if (!iniFile.KeyExists("CoefMinPart", "Params"))
                errorCfgFile.Add("CoefMinPart clé introuvable");
            else
            {
                CoefMinPart = MyCdbl(iniFile.Read("CoefMinPart", "Params"));
                if (CoefMinPart == 0)
                    errorCfgFile.Add("CoefMinPart valeur erronée");
            }

            if (!iniFile.KeyExists("MinDistPart", "Params"))
                errorCfgFile.Add("MinDistPart clé introuvable");
            else
            {
                MinDistPart = MyCdbl(iniFile.Read("MinDistPart", "Params"));
                if (MinDistPart == 0)
                    errorCfgFile.Add("MinDistPart valeur erronée");
            }

            if (!iniFile.KeyExists("StepIteration", "Params"))
                errorCfgFile.Add("StepIteration clé introuvable");
            else
            {
                StepIteration = MyCdbl(iniFile.Read("StepIteration", "Params"));
                if (StepIteration == 0)
                    errorCfgFile.Add("StepIteration valeur erronée");
            }

            if (!iniFile.KeyExists("MinDistMire", "Params"))
                errorCfgFile.Add("MinDistMire key clé introuvable");
            else
            {
                MinDistMire = MyCdbl(iniFile.Read("MinDistMire", "Params"));
                if (MinDistMire == 0)
                    errorCfgFile.Add(" MinDistMire valeur erronée");
            }

            if (errorCfgFile.Count > 0)
            {
                ViewLog(errorCfgFile);
                return;
            }
            #endregion CfgFile Control

            //  args must be agiFileName NestKey or -redo
            //  "C:\alma\Actcut39\Alma_NTX\imb\agi\512.agi" "KEY_NEST_1"
            if (args.Count() != 2)
                return;

            string agiFileName = args[0];
            string nestKey = args[1];

            config = ConfigurationManager.OpenExeConfiguration(executablePath);
            if (config.AppSettings.Settings["agiFileName"] != null)
                config.AppSettings.Settings["agiFileName"].Value = agiFileName;
            else
                config.AppSettings.Settings.Add("agiFileName", agiFileName);

            if (config.AppSettings.Settings["nestKey"] != null)
                config.AppSettings.Settings["nestKey"].Value = nestKey;
            else
                config.AppSettings.Settings.Add("nestKey", nestKey);

            config.Save(ConfigurationSaveMode.Full, true);


            Console.WriteLine("Gestion des mires");

            List<Tuple<string, double[], double[]>> res = FindParts(agiFileName, nestKey);
            Tuple<string, double[], double[]> ares = res[0];
            double[] p;

            if (ares.Item2[2] == -1)
            {
                //  si mire 1 pas trouvée, on cherche sur la tôle
                p = findSheetPosition(1);
                ares = new Tuple<string, double[], double[]>(ares.Item1, p, ares.Item3);
            }
            if (ares.Item3[2] == -1)
            {
                //  si mire 2 pas trouvée, on cherche sur la tôle
                p = findSheetPosition(2);
                ares = new Tuple<string, double[], double[]>(ares.Item1, ares.Item2, p);
            }

            double[] p1 = ares.Item2;   //  point 1
            double[] p2 = ares.Item3;   //  point 2

            if (p1[1] == -2 && p2[1] == -2)
            {
                //  pas de mire trouvée
            }
            else
            {
                //  calcul de la distance des 2 mires trouvees
                double d = Math.Sqrt(Math.Pow((p2[0] - p1[0]), 2) + Math.Pow(p2[1] - p1[1], 2));
                if (d < MinDistMire)
                {
                    Console.WriteLine("Distance mire minimum non respectée");
                    p = findSheetPosition(3);
                    ares = new Tuple<string, double[], double[]>(ares.Item1, ares.Item2, p);
                }
            }
            Console.WriteLine();

            res[0] = ares;

            Console.WriteLine("Mire trouvée");
            Console.WriteLine(res.First().Item1 + " (" +
                string.Join(" ", res.First().Item2.Select(z => z.ToString())) + ") (" +
                string.Join(" ", res.First().Item3.Select(z => z.ToString())) + ")");

            resultFile.WriteLine("\"" + agiFileName + "\"");
            logFile.WriteLine("\"" + agiFileName + "\"");

            foreach (var t in res)
            {
                resultFile.WriteLine("\"" + t.Item1 + "\"");
                resultFile.WriteLine(string.Join(" ", t.Item2.Select(z => z.ToString())));
                resultFile.WriteLine(string.Join(" ", t.Item3.Select(z => z.ToString())));

                logFile.WriteLine("\"" + t.Item1 + "\" ");
                logFile.WriteLine("(" + string.Join(" ", t.Item2.Select(z => z.ToString())) + ")");
                logFile.WriteLine("(" + string.Join(" ", t.Item3.Select(z => z.ToString())) + ")");
            }
            Console.WriteLine("fermeture dans 3 secondes");
            Thread.Sleep(3000);
            //Console.WriteLine("Appuyez une touche pour continuer . . .");
            //Console.ReadKey(true);
        }

        private static double[] findSheetPosition(int mireNumber)
        {
            P01ImporterPtr importer = new P01ImporterPtr();
            PartFactoryPtr factory = new PartFactoryPtr();
            importer = NewP01Importer(pfile);
            AssociateCpfMachiningToAllProfilesFromP01(importer, Machining.CutType);
            factory = NewP01PartFactory(importer);
            double[] p = new double[] { -1, -1, -1 };
            bool gtr = false;
            double astep = StepIteration / 4;
            if (mireNumber == 1)
            {
                Console.Write("Processus recherche dans tôle");
                logFile.WriteLine("Sheet search");

                //  recherche départ BG horizontal de bas en haut
                for (int i = (int)DimMire; i < (int)(lgutil - DimMire); i += (int)astep)
                {
                    for (int j = (int)DimMire; j < (int)(sheetHeight - DimMire); j += (int)astep)
                    {
                        if (IsInside(i, j, factory) != -1)
                        {
                            p = new double[] { i, j, 1 };
                            gtr = true;
                            break;
                        }
                        else
                            logFile.WriteLine("    Sheet search :Target 1 Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                    }
                    if (gtr)
                        break;
                }
            }
            if (mireNumber == 2)
            {
                Console.Write("Processus recherche dans tôle");
                logFile.WriteLine("Sheet search");

                //  recherche départ BD horizontal de bas en haut
                for (int i = (int)(lgutil - DimMire); i > (int)DimMire; i -= (int)astep)
                {
                    for (int j = (int)DimMire; j < (int)(sheetHeight - DimMire); j += (int)astep)
                    {
                        if (IsInside(i, j, factory) != -1)
                        {
                            p = new double[] { i, j, 1 };
                            gtr = true;
                            break;
                        }
                        else
                            logFile.WriteLine("    Sheet search :Target 2 Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                    }
                    if (gtr)
                        break;
                }
            }

            if (mireNumber == 3)
            {
                Console.Write("Processus recherche dans tôle");
                logFile.WriteLine("Sheet search");


                //  recherche départ HD horizontal de haut en bas 
                for (int i = (int)(lgutil - DimMire); i > (int)DimMire; i -= (int)astep)
                {
                    for (int j = (int)(sheetHeight - DimMire); j > (int)DimMire; j -= (int)astep)
                    {
                        if (IsInside(i, j, factory) != -1)
                        {
                            p = new double[] { i, j, 1 };
                            gtr = true;
                            break;
                        }
                        else
                            logFile.WriteLine("    Sheet search :Target 2 Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                    }
                    if (gtr)
                        break;
                }
            }
            if (p != new double[] { -1, -1, -1 })
                Console.WriteLine("  Point Ok(X = " + p[0].ToString("#.0####") + " Y=" + p[1].ToString("#.0####") + ")");
            //Console.WriteLine("  " + string.Join(" ", p.Select(z => z.ToString())));
            else
                Console.WriteLine();
            return p;
        }

        private static void ViewLog(List<string> errorCfgFile)
        {
            string tmpFile = Path.GetTempFileName();
            using (StreamWriter sw = new StreamWriter(tmpFile))
            {
                foreach (string line in errorCfgFile)
                {
                    sw.WriteLine(line);
                }
            }
            Process.Start("notepad.exe", tmpFile);
        }

        private static List<Tuple<string, double[], double[]>> FindParts(string agiFileName, string nestKey)
        {
            string dirName = Path.GetDirectoryName(agiFileName);
            string fileName = Path.GetFileName(agiFileName);
            string ext = Path.GetExtension(agiFileName);

            AgiRin aagi = new AgiRin();

            aagi.ReadAgiFile(dirName + Path.DirectorySeparatorChar + fileName, false, "Export");

            AgiParts parts = aagi.Parts;
            AgiFormats sheets = aagi.Formats;
            AgiNesting anest = aagi.Nestings[nestKey];
            double[] p1 = new double[] { -1, -1, -1 };
            double[] p2 = new double[] { -1, -1, -1 };

            bool gtr1 = false;
            bool gtr2 = false;
            bool isbinpart = false;
            P01ImporterPtr importer = new P01ImporterPtr();
            PartFactoryPtr factory = new PartFactoryPtr();
            List<Tuple<string, double[], double[]>> resultTab = new List<Tuple<string, double[], double[]>>();

            try
            {
                string anestingname = anest.Name;
                List<AgiPosition> SortedPos = new List<AgiPosition>();

                logFile.WriteLine("Nesting -> " + Path.GetFileNameWithoutExtension(anestingname));
                for (int i = 1; i < anest.NestResults.Count; i++)
                {
                    if ((string)anest.NestResults[i].part.GetFieldValue("Size") == "B")
                    {
                        isbinpart = true;
                        break;
                    }
                }
                if (!isbinpart)
                {
                    p1 = new double[] { -2, -2, -2 };
                    p2 = new double[] { -2, -2, -2 };
                    gtr1 = true;
                    gtr2 = true;
                    Console.WriteLine("No bin parts in the nesting");
                    logFile.WriteLine("No bin parts in the nesting");
                }

                if (!gtr1)
                {
                    pfile = DefineAph.GetKeyPath("nesting") + anestingname.Replace(".r", ".p");
                    importer = NewP01Importer(pfile);
                    AssociateCpfMachiningToAllProfilesFromP01(importer, Machining.CutType);
                    factory = NewP01PartFactory(importer);
                    lgutil = anest.utilLength;
                    List<AgiPosition> TabPos = new List<AgiPosition>();
                    foreach (AgiPosition agiPosition in anest.Positions)
                        TabPos.Add(agiPosition);
                    SortedPos = TabPos.OrderBy(p => p.BoxX).ToList();
                }
                sheetHeight = anest.Format.DimY;
                Console.WriteLine("Nesting " + anestingname + "\n");

                while (!(gtr1 & gtr2))
                {
                    string savekey1 = "";
                    string savekey2 = "";
                    double maxPosY1 = 0;
                    double maxPosY2 = 0;
                    double saveX1 = 9999999;    //  mire 1
                    double saveX2 = 0;          //  mire 2
                    double dmin = 999999;
                    AgiPosition apart1 = null;
                    AgiPosition apart2 = null;

                    int ind = 0;
                    int ind1 = -1;
                    int ind2 = -1;

                    foreach (AgiPosition apos in SortedPos)
                    {
                        //Console.WriteLine(apos.Key + " (Y=" + apos.BoxY.ToString("#.0####") + ")");
                        if (!gtr1)
                        {
                            if (apos.BoxDimX > CoefMinPart * DimMire & apos.BoxDimY > CoefMinPart * DimMire)
                            {
                                double d = Math.Sqrt(Math.Pow(apos.BoxX, 2) + Math.Pow(apos.BoxY, 2));
                                if (d < dmin)
                                {
                                    savekey1 = apos.Key;
                                    saveX1 = apos.BoxX;
                                    ind1 = ind;
                                    maxPosY1 = apos.BoxY + apos.BoxDimY;
                                    dmin = d;
                                }
                            }
                            else
                            {
                                //Console.WriteLine("Part in " + parts[apos.PartKey].FileName + " (X=" +
                                //    apos.BoxX.ToString("#.0####") + " Y=" + apos.BoxY.ToString("#.0####") + ") too thin for target 1");
                                logFile.WriteLine("Part in " + parts[apos.PartKey].FileName + " (X=" +
                                    apos.BoxX.ToString("#.0####") + " Y=" + apos.BoxY.ToString("#.0####") + ") too thin for target 1");
                            }
                        }
                        if (!gtr2)
                        {
                            if (apos.BoxDimX > CoefMinPart * DimMire & apos.BoxDimY > CoefMinPart * DimMire)
                            {
                                if (apos.BoxX + apos.BoxDimX > saveX2)
                                {
                                    savekey2 = apos.Key;
                                    saveX2 = apos.BoxX + apos.BoxDimX;
                                    ind2 = ind;
                                    maxPosY2 = apos.BoxY;
                                }
                            }
                        }
                        else
                        {
                            //Console.WriteLine("Part in " + parts[apos.PartKey].FileName + " (X=" +
                            //    apos.BoxX.ToString("#.0####") + " Y=" + apos.BoxY.ToString("#.0####") + ") too thin for target 2");
                            logFile.WriteLine("Part in " + parts[apos.PartKey].FileName + " (X=" +
                                apos.BoxX.ToString("#.0####") + " Y=" + apos.BoxY.ToString("#.0####") + ") too thin for target 2");
                        }
                        ind++;
                    }
                    bool searchTwice = false;
                    if (savekey1 == savekey2) //  same part 
                    {
                        searchTwice = true;
                        logFile.WriteLine("Search in the same part");
                    }

                    if (!gtr1)
                    {
                        if (savekey1 == "")
                        {
                            p1 = new double[] { -1, -1, -1 };
                            logFile.WriteLine("No part found for target 1");
                            Console.WriteLine("Pas de pièce trouvée pour cible 1");
                            gtr1 = true;
                        }
                        else
                        {
                            apart1 = anest.Positions[savekey1];
                            logFile.Write("target 1 part " + parts[apart1.PartKey].FileName);
                            for (int j = (int)(apart1.BoxY + DimMire * MinDistPart); j < (int)(apart1.BoxY + apart1.BoxDimY); j += (int)((apart1.BoxDimY + DimMire * MinDistPart) / StepIteration))
                            {
                                for (int i = (int)(apart1.BoxX + DimMire * MinDistPart); i < (int)(apart1.BoxX + apart1.BoxDimX) - (int)(DimMire * MinDistPart); i += (int)(apart1.BoxDimX / StepIteration))
                                {
                                    if (IsInside(i, j, factory) != 1)
                                    {
                                        p1 = new double[] { i, j, 1 };
                                        gtr1 = true;
                                        logFile.WriteLine("    Point Ok (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                        break;
                                    }
                                    else
                                    {
                                        logFile.WriteLine("    Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                    }
                                }
                                if (gtr1)
                                    break;
                            }

                            if (searchTwice)
                            {
                                for (int j = (int)(apart1.BoxY + apart1.BoxDimY - DimMire * MinDistPart); j > (int)(apart1.BoxY); j -= (int)((apart1.BoxDimY + DimMire * MinDistPart) / StepIteration))
                                {
                                    for (int i = (int)(apart1.BoxX + apart1.BoxDimX - (DimMire * MinDistPart)); i > (int)(apart1.BoxX + (DimMire * MinDistPart)); i -= (int)(apart1.BoxDimX / StepIteration))
                                    {
                                        if (IsInside(i, j, factory) != 1)
                                        {
                                            gtr2 = true;
                                            p2 = new double[] { i, j, 1 };
                                            logFile.WriteLine("    Point Ok (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                            break;
                                        }
                                        else
                                        {
                                            logFile.WriteLine("    Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                        }
                                    }
                                    if (gtr2)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!gtr2)
                    {
                        if (savekey2 == "")
                        {
                            p2 = new double[] { -1, -1, -1 };
                            logFile.WriteLine("No part found for target 2");
                            Console.WriteLine("Pas de pièce trouvée pour cible 2");
                            gtr2 = true;
                        }
                        else
                        {
                            if (!searchTwice)
                            {
                                apart2 = anest.Positions[savekey2];
                                logFile.Write("target 2 part " + parts[apart2.PartKey].FileName);
                                for (int j = (int)(apart2.BoxY + apart2.BoxDimY - DimMire * MinDistPart); j > (int)(apart2.BoxY); j -= (int)((apart2.BoxDimY + DimMire * MinDistPart) / StepIteration))
                                {
                                    for (int i = (int)(apart2.BoxX + apart2.BoxDimX - (DimMire * MinDistPart)); i > (int)(apart2.BoxX + (DimMire * MinDistPart)); i -= (int)(apart2.BoxDimX / StepIteration))
                                    {
                                        if (IsInside(i, j, factory) != 1)
                                        {
                                            gtr2 = true;
                                            p2 = new double[] { i, j, 1 };
                                            logFile.WriteLine("    Point Ok (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                            break;
                                        }
                                        else
                                        {
                                            logFile.WriteLine("    Point overlap (X=" + i.ToString("#.0####") + " Y=" + j.ToString("#.0####") + ")");
                                        }
                                    }
                                    if (gtr2)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!gtr1)
                    {
                        logFile.WriteLine("Target 1 No place found in part " + parts[apart1.PartKey].FileName);
                        SortedPos.RemoveAt(ind1);
                        ind2 -= 1;
                    }
                    if (!searchTwice)
                    {
                        if (!gtr2)
                        {
                            logFile.WriteLine("Target 2 No place found in part " + parts[apart2.PartKey].FileName);
                            SortedPos.RemoveAt(ind2);
                        }
                    }
                }
                resultTab.Add(new Tuple<string, double[], double[]>(anest.Key.ToString(), p1, p2));
            }
            catch
            {
                resultTab.Add(new Tuple<string, double[], double[]>(anest.Key.ToString(), p1, p2));
            }
            aagi.TearDown();
            return resultTab;
        }

        private static double MyCdbl(string value)
        {
            value = value.Replace(".", decimaSeparator).Replace(",", decimaSeparator);
            double.TryParse(value, out double result);
            return result;
        }
        private static int IsInside(int i, int j, PartFactoryPtr factory)
        {
            return IsOnProfile(factory, i, j, DimMire / 2, true);
        }
    }
}
