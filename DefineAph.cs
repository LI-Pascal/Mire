using System.Text;
using System.Runtime.InteropServices;

namespace Actcut
{
    public static class DefineAph
    {
        const int utilStrMaxLen = 512;
        #region Dll call function
        #region RSC
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getSetupRscPath")]
        private static extern int APH_GetSetupRscPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getRscPath")]
        private static extern int APH_GetRscPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_setRscPath")]
        private static extern int APH_SetRscPath(string path);
        #endregion

        #region DATA
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getSetupContextPath")]
        private static extern int APH_GetSetupContextPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getContextPath")]
        private static extern int APH_GetContextPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_setContextPath")]
        private static extern int APH_SetContextPath(string path);
        #endregion

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getKeyPath")]
        private static extern int APH_GetKeyPath(string key, StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getTempPath")]
        private static extern int APH_GetTempPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getAppPath")]
        private static extern int APH_GetAppPath(StringBuilder appPath, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getBinPath")]
        private static extern int APH_GetBinPath(StringBuilder filename, int maxLength);

        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getCurrentContext")]
        private static extern int APH_GetCurrentContext(StringBuilder context, int maxLength);
        #endregion

        #region public calls
        public static string GetKeyPath(string key)
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetKeyPath(key, s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetBinPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetBinPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetAppPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetAppPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetSetupContextPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetSetupContextPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetContextPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetContextPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string SetContextPath(string path)
        {
            if (APH_SetContextPath(path) != 0)
                return path;
            else
                return string.Empty;
        }
        public static string GetRscPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetRscPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetSetupRscPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetSetupRscPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string SetRscPath(string path)
        {
            if (APH_SetRscPath(path) != 0)
                return path;
            else
                return string.Empty;
        }
        /// <summary>
        /// won't works
        /// </summary>
        /// <returns></returns>
        public static string GetCurentContext()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetCurrentContext(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string GetTempPath()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (APH_GetTempPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        private static string ResizeNullTerminatedStringNoTrim(StringBuilder str)
        {
            var result = (str.ToString()).TrimEnd('\0').TrimEnd();
            return result;
        }
        #endregion
    }
}
